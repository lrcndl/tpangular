import { Component, OnInit } from '@angular/core';
import { DemoService } from '../services/demo/demo.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  private url="http://newsapi.org/v2/top-headlines?country=fr&category=technology&apiKey=cae6828bb86e439fa1cd7988ee475548";
  private data:Object;
  constructor(private _demo:DemoService) { }


  ngOnInit() {
    console.log('etape1');
    let result = this._demo.get(this.url);
    result.subscribe(_data =>{
      console.log(_data.articles[0]);
      console.log("Etape resultat=ok");
      this.isload =true;
      console.log(this.isload);
      this.data =_data.articles;
      this.author =_data.articles.author;
      this.title =_data.articles.title;
      this.description =_data.articles.description;
      this.url =_data.articles.url;
      this.urlToImage =_data.articles.urlToImage;
      this.publishedAt =_data.articles.publishedAt;
      this.content =_data.articles.content;
    });
  }

    

}
