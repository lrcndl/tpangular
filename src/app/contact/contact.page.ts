import { Component, OnInit } from '@angular/core';
import { DemoService } from '../services/demo/demo.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  private url = "https://www.prevision-meteo.ch/services/json/lyon";
  private data: Object;
  constructor(private _demo: DemoService) { }

  ngOnInit() {// ajout de la météo
    console.log('etape1');
    let result = this._demo.get(this.url);
    result.subscribe(_data => {
      console.log(_data.city_info);
      console.log(_data.current_condition);
      console.log(_data.fcst_day_0);
      this.isload = true;
      console.log(this.isload);
      console.log("Etape resultat=ok");
      this.data = _data;
      this.name = _data.city_info.name;
      this.country = _data.city_info.country;
      this.latitude = _data.city_info.latitude;
      this.elevation = _data.city_info.elevation;
      this.sunrise = _data.city_info.sunrise;
      this.sunset = _data.city_info.sunset;
      this.icon_big = _data.current_condition.icon_big;
      this.date = _data.current_condition.date;
      this.hour = _data.current_condition.hour;
      this.condition = _data.current_condition.condition;
      this.tmin = _data.fcst_day_0.tmin;
      this.tmax = _data.fcst_day_0.tmax;
      navigator.geolocation.getCurrentPosition(function (position) {
        console.log(position);
        showPosition(position);//montrer la position
      });

      var x = document.getElementById("demo");
      function getLocation() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(showPosition);
        } else {
          x.innerHTML = "Geolocation is not supported by this browser.";
        }
      }
      function showPosition(position) {// montrer latitude et longitude
        x.innerHTML = "Latitude: " + position.coords.latitude +
          "<br>Longitude: " + position.coords.longitude;

        var latlon = position.coords.latitude + "," + position.coords.longitude;
        //image de ma localisation
        var img_url = "https://maps.googleapis.com/maps/api/staticmap?center=" + latlon + "&zoom=14&size=400x300&sensor=false&key=AIzaSyCZJwauN9_GN5zVqvz01bHBjf8zT-97Bw4";
        console.log(img_url);
        let img = <HTMLImageElement>document.getElementById("map");
        img.src = img_url;
      }
    });
  }

    openCamera() {
      let video = <HTMLVideoElement>document.getElementById("video");

      navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
        video.srcObject = stream;
        video.play();
      });
    }
  
}
