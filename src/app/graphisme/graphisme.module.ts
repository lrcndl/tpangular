import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GraphismePageRoutingModule } from './graphisme-routing.module';

import { GraphismePage } from './graphisme.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GraphismePageRoutingModule
  ],
  declarations: [GraphismePage]
})
export class GraphismePageModule {}
