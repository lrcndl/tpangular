import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GraphismePage } from './graphisme.page';

const routes: Routes = [
  {
    path: '',
    component: GraphismePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GraphismePageRoutingModule {}
