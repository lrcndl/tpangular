import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GraphismePage } from './graphisme.page';

describe('GraphismePage', () => {
  let component: GraphismePage;
  let fixture: ComponentFixture<GraphismePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphismePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GraphismePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
