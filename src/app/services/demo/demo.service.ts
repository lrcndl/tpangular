import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DemoService {

  constructor(private http: HttpClient) { 
    console.log('INIT --- SERVICE DEMO ---')
  }
get(url:string){
    return this.http.get(url);
  }
}
